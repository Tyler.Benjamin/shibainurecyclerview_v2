package com.example.shibainurecyclerv21.repository

import com.example.shibainurecyclerv21.util.Resource

/**
 * Repository implementation.
 *
 * @constructor Create empty Repository
 */
interface Repository {
    /**
     * Get list.
     *
     * @return
     */
    suspend fun getList(): Resource<List<String>>
}
