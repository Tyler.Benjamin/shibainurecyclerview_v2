package com.example.shibainurecyclerv21.model.remote

import retrofit2.Response
import retrofit2.http.GET

/**
 * Api service for Retrieving Data.
 *
 *
 * @constructor Create empty Api service
 */
interface ApiService {

    @GET(SHIBA_PATH)
    suspend fun getList(): Response<List<String>>

    companion object {
        private const val SHIBA_PATH = "shibes?count=10"
    }
}
