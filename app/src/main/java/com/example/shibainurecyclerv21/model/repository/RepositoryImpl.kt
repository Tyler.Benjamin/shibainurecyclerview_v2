package com.example.shibainurecyclerv21.model.repository

import com.example.shibainurecyclerv21.model.remote.ApiService
import com.example.shibainurecyclerv21.repository.Repository
import com.example.shibainurecyclerv21.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

/**
 * Repository impl for calling to APi.
 *
 * @property apiInstance
 * @constructor Create empty Repository impl
 */
class RepositoryImpl @Inject constructor(private var apiInstance: ApiService) : Repository {
    override suspend fun getList(): Resource<List<String>> = withContext(Dispatchers.IO) {
        return@withContext try {
            val res = apiInstance.getList()
            if (res.isSuccessful && res.body() != null) {
                Resource.Success(res.body()!!)
            } else {
                Resource.Error("An Error Occurred")
            }
        } catch (e: IllegalArgumentException) {
            Resource.Error(e.message.toString())
        }
    }
}
