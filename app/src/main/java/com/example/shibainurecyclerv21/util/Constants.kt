package com.example.shibainurecyclerv21.util

/**
 * Constants value for app.
 *
 * @constructor Create empty Constants
 */
object Constants {
    const val BASE_URL = "http://shibe.online/api/"
}
