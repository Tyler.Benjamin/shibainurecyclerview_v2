package com.example.shibainurecyclerv21.util

/**
 * Resource for holding datat from API request.
 *
 * @param T
 * @constructor
 *
 * @param data
 * @param message
 */
sealed class Resource<T>(data: T? = null, message: String? = null) {
    /**
     * Success.
     *
     * @param T
     * @property data
     * @constructor Create empty Success
     */
    data class Success<T>(val data: T) : Resource<T>(data)

    /**
     * Loading.
     *
     * @param T
     * @constructor Create empty Loading
     */
    class Loading<T> : Resource<T>()

    /**
     * Error.
     *
     * @param T
     * @property message
     * @constructor Create empty Error
     */
    data class Error<T>(val message: String) : Resource<T>(message = message)
}
