package com.example.shibainurecyclerv21

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Shiba Application File.
 *
 * @constructor Create empty Shiba app
 */
@HiltAndroidApp
class ShibaApp : Application()
