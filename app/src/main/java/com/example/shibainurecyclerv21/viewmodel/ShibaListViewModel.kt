package com.example.shibainurecyclerv21.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.shibainurecyclerv21.model.repository.RepositoryImpl
import com.example.shibainurecyclerv21.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Shiba list view model.
 *
 * @property repo
 * @constructor Create empty Shiba list view model
 */
@HiltViewModel
class ShibaListViewModel @Inject constructor(private val repo: RepositoryImpl) : ViewModel() {
    private val _viewState: MutableLiveData<Resource<List<String>>> = MutableLiveData()
    val viewState: LiveData<Resource<List<String>>> = _viewState

    init {
        getList()
    }

    private fun getList() = viewModelScope.launch {
        _viewState.value = repo.getList()
    }
}
