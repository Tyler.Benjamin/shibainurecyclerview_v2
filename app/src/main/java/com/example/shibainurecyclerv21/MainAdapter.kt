package com.example.shibainurecyclerv21

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.shibainurecyclerv21.databinding.ItemListBinding

/**
 * Main adapter for [RecyclerView].
 *
 * @constructor Create empty Main adapter
 */
class MainAdapter : RecyclerView.Adapter<MainAdapter.ShibuViewHolder>() {
    private var shibuList: List<String> = mutableListOf()

    /**
     * Shibu view holder.
     *
     * @property binding
     * @constructor Create empty Shibu view holder
     */
    inner class ShibuViewHolder(
        private val binding: ItemListBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        /**
         * Apply shibu list of strings to recycler view.
         *
         * @param shibu
         */
        fun applyShibu(shibu: String) = with(binding) {
            imageView.load(shibu)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShibuViewHolder {
        val binding = ItemListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ShibuViewHolder(binding)
    }

    /**
     * On bind view holder.
     *
     * @param holder
     * @param position
     */
    override fun onBindViewHolder(holder: ShibuViewHolder, position: Int) {
        val item = shibuList[position]
        holder.applyShibu(item)
    }

    /**
     * Get item count.
     *
     */
    override fun getItemCount() = shibuList.size

    /**
     * Add items fucntion.
     *
     * @param shibuList
     */
    fun addItems(shibuList: List<String>) {
        this.shibuList = shibuList
    }
}
