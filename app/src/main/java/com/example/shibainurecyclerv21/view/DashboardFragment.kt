package com.example.shibainurecyclerv21.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.shibainurecyclerv21.MainAdapter
import com.example.shibainurecyclerv21.databinding.FragmentDashboardBinding
import com.example.shibainurecyclerv21.util.Resource
import com.example.shibainurecyclerv21.viewmodel.ShibaListViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * Dashboard fragment Entry.
 *
 * @constructor Create empty Dashboard fragment
 */
@AndroidEntryPoint
@Suppress("VariableNaming", "ObjectPropertyNaming")
class DashboardFragment : Fragment() {
    var _binding: FragmentDashboardBinding? = null
    val binding: FragmentDashboardBinding get() = _binding!!
    private val viewModel by viewModels<ShibaListViewModel>()
//    private val charAdapter: CharAdapter by lazy { CharAdapter(::navigateToDetails) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentDashboardBinding.inflate(
        inflater,
        container,
        false
    )
        .also {
            _binding = it
        }
        .root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() = with(binding) {
//        rvView.adapter = charAdapter
        viewModel.viewState.observe(viewLifecycleOwner) { viewState ->
            when (viewState) {
                is Resource.Error -> viewState.message
                is Resource.Loading -> {
                    //
                    println("initViews: error")
                }
                is Resource.Success ->
                    rvView.adapter =
                        MainAdapter().apply { addItems(viewState.data) }
            }
        }
    }
}
